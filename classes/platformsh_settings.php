<?php

/**
 * @package ccPlatformSh
 * @author  Serhey Dolgushev <serhey@contextualcode.com>
 * @date    23 Aug 2017
 */

class ccPlatformShSettings {

    /**
     * A base64-encoded JSON object whose keys are the relationship name
     * and the values are arrays of relationship endpoint definitions.
     *
     * @var string
     */
    const RELATIONSHIPS = 'PLATFORM_RELATIONSHIPS';

    /**
     * A base64-encoded JSON object which keys are variables names and
     * values are variable values.
     *
     * @var string
     */
    const VARIABLES = 'PLATFORM_VARIABLES';

    /**
     * A base64-encoded JSON object that describes the routes that
     * you defined in the environment
     *
     * @var string
     */
    const ROUTES = 'PLATFORM_ROUTES';

    /**
     * Main DB service endpoint
     *
     * @var string
     */
    const ENDPOINT_DB = 'database';

    /**
     * Main Machform DB service endpoint
     *
     * @var string
     */
    const ENDPOINT_MACHFORM_DB = 'database_machform';

    /**
     * SOLR service endpoint
     *
     * @var string
     */
    const ENDPOINT_SOLR = 'solr';

    /**
     * Solr languages cores map variable
     *
     * @var string
     */
    const VARIABLE_SOLR_LANGUAGES_CORES_MAP = 'SOLR_LANGUAGES_CORES_MAP';

    /**
     * @var ccPlatformShSettings
     */
    private static $instance = null;

    /**
     * Singleton
     *
     * @return ccPlatformShSettings
     */
    public static function getInstance() {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * Decodes value from relationships env variable
     *
     * @return array()
     */
    protected function getRelationships() {
        $tmp = getenv(self::RELATIONSHIPS);
        return json_decode(base64_decode($tmp), true);
    }

    /**
     * Decodes value from env variables
     *
     * @return array()
     */
    protected function getVariables() {
        $tmp = getenv(self::VARIABLES);
        return json_decode(base64_decode($tmp), true);
    }

    /**
     * Decodes value from routes env variables
     *
     * @return array()
     */
    public function getRoutes() {
        $tmp = getenv(self::ROUTES);
        return json_decode(base64_decode($tmp), true);
    }

    /**
     * Extracts settings from env variable
     *
     * @return array()
     */
    public function getSettings() {
        $return = array();

        $dbSettings = $this->getDatabaseSettings();
        if ($dbSettings !== null) {
            $return['site.ini'] = array(
                'DatabaseSettings' => array(
                    'Database' => $dbSettings['name'],
                    'User'     => $dbSettings['user'],
                    'Password' => $dbSettings['pass'],
                    'Server'   => $dbSettings['host'],
                    'Port'     => $dbSettings['port']
                )
            );
            $return['file.ini'] = array(
                'eZDFSClusteringSettings' => array(
                    'DBName'     => 'cluster',
                    'DBUser'     => $dbSettings['user'],
                    'DBPassword' => $dbSettings['pass'],
                    'DBHost'     => $dbSettings['host'],
                    'DBPort'     => $dbSettings['port']
                )
            );
        }

        $machformDbSettings = $this->getMachformDatabaseSettings();
        if ($machformDbSettings !== null) {
            $return['ccmachform.ini'] = array(
                'MachformDatabase' => array(
                    'Name'     => 'machform',
                    'Username' => $machformDbSettings['user'],
                    'Password' => $machformDbSettings['pass'],
                    'Host'     => $machformDbSettings['host'],
                    'Port'     => $machformDbSettings['port'],
                    'FormTable' => 'ap_forms',
                    'UserTable' => 'ap_users'
                )
            );
        }

        $solrSettings = $this->getSolrSettings();
        if ($solrSettings !== null) {
            $coresMap = $this->getSolrLanguagesCoresMap($solrSettings);
            $return['ezfind.ini'] = array(
                'LanguageSearch' => array(
                    'DefaultCore'       => $solrSettings['core'],
                    'LanguagesCoresMap' => $coresMap
                )
            );

            $baseUrl = 'http://' . $solrSettings['host'] . ':' . $solrSettings['port'] . '/solr';
            $shards = array();
            foreach ($coresMap as $locale => $core) {
                if (isset($shards[$core]) === false) {
                    $shards[$core] = $baseUrl . '/' . $core;
                }
            }
            $return['solr.ini'] = array(
                'SolrBase' => array(
                    'SearchServerURI' => $baseUrl,
                    'Shards'          => $shards
                )
            );
        }

        return $return;
    }

    /**
     * Extracts database settings. We are assuming, that platform.sh cluster database name is "cluster".
     * And it is using the same endpoint as main database. So database service definition is similar to:
     *
     * mysqldb:
     *     type: mysql:10.0
     *     disk: 768
     *     configuration:
     *         schemas:
     *             - main
     *             - cluster
     *         endpoints:
     *             mysql:
     *                 default_schema: main
     *                 privileges:
     *                     main: admin
     *                     cluster: admin
     *
     * @return array()
     */
    public function getDatabaseSettings() {
        $relationships = $this->getRelationships();
        if (is_array($relationships) === false || isset($relationships[self::ENDPOINT_DB]) === false) {
            return null;
        }

        $endpoint = $relationships[self::ENDPOINT_DB][0];
        return array(
            'name' => $endpoint['path'],
            'user' => $endpoint['username'],
            'pass' => $endpoint['password'],
            'host' => $endpoint['host'],
            'port' => $endpoint['port']
        );
    }

    public function getMachformDatabaseSettings() {
        $relationships = $this->getRelationships();
        if (is_array($relationships) === false || isset($relationships[self::ENDPOINT_MACHFORM_DB]) === false) {
            return null;
        }
        
        $endpoint = $relationships[self::ENDPOINT_MACHFORM_DB][0];
        return array(
            'name' => $endpoint['path'],
            'user' => $endpoint['username'],
            'pass' => $endpoint['password'],
            'host' => $endpoint['host'],
            'port' => $endpoint['port']
        );
    }

    /**
     * Extracts solr settings.
     *
     * @return array()
     */
    public function getSolrSettings() {
        $relationships = $this->getRelationships();
        if (is_array($relationships) === false || isset($relationships[self::ENDPOINT_SOLR]) === false) {
            return null;
        }

        $endpoint = $relationships[self::ENDPOINT_SOLR][0];
        return array(
            'host' => $endpoint['host'],
            'port' => $endpoint['port'],
            'core' => substr($endpoint['path'], 5)
        );
    }

    /**
     * Returns Solr languages cores map
     *
     * @param $solrSettings
     * @return array()
     */
    protected function getSolrLanguagesCoresMap($solrSettings) {
        $map = 'eng-US,' . $solrSettings['core'];
        $tmp = getenv(self::VARIABLE_SOLR_LANGUAGES_CORES_MAP);
        if ($tmp) {
            $map = $tmp;
        }

        $coresMap = array();
        foreach (explode(';', $map) as $localeMap) {
            $tmp = explode(',', $localeMap);
            if (count($tmp) !== 2) {
                continue;
            }

            $coresMap[$tmp[0]] = $tmp[1];
        }

        return $coresMap;
    }
}
